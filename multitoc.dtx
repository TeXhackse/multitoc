%% \iffalse meta-comment
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Copyright (C) Martin Schröder, 1998–1999
% 				Marei Peischl (peiTeX)  <marei@peitex.de>, 2024
%
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either version 1.3c
% of this license or (at your option) any later version.
% The latest version of this license is in
%    http://www.latex-project.org/lppl.txt
% and version 1.3c or later is part of all distributions of LaTeX
% version 2005/12/01 or later.
%
% This work has the LPPL maintenance status `maintained'.
% This package is maintained but will not receive any additional features.
%
% The Current Maintainer of this work is
%   Marei Peischl <marei@peitex.de>.
%
%
% This work consists of the files
%    README.md
%    multitoc.dtx
%    multitoc.ins
% and the derived file
%    multitoc.sty.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% \fi
% \iffalse               
\NeedsTeXFormat{LaTeX2e}[2015-09-18]
%<package>\ProvidesPackage{multitoc}
%<package>        [2024/06/13 v2.02 MultiToc Package]
%
%<*driver>
\ProvidesFile{multitoc.drv}
      [1999/06/08 v2.02 Driver for MultiToc Package]
\documentclass{ltxdoc}
\usepackage{url}
\usepackage{ragged2e}
\usepackage[toc]{multitoc}
\GetFileInfo{multitoc.sty}
\makeatletter
\IfFileExists{gitHeadInfo.gin}{
   \newcommand*{\GI@githeadinfo@file}{gitHeadInfo.gin}
}{}
\makeatother
\usepackage{gitinfo2}
\setcounter{IndexColumns}{2}
\EnableCrossrefs
%%\DisableCrossrefs% Say \DisableCrossrefs if index is ready
\CodelineIndex    % Index code by line number
\OnlyDescription  % comment out for implementation details
%%\OldMakeIndex    % use if your MakeIndex is pre-v2.9
\setcounter{IndexColumns}{2}
\setlength{\IndexMin}{30ex}
\setlength{\premulticols}{\IndexMin}
\RaggedRight
\begin{document}
   \DocInput{multitoc.dtx}
\end{document}
%</driver>
% \fi
%
%  \changes{v0.90}{1998/05/23}{New}
%  \changes{v1.00}{1998/08/09}{Documentation improved}
%  \changes{v2.00}{1998/08/23}{Redesign}
%  \changes{v2.01}{1999/06/08}{Moved to LPPL}
%
%  \newcommand*{\option}[1]{\textnormal{\sffamily#1}}
%  \newcommand*{\package}[1]{\textnormal{\sffamily#1}}
%  \newcommand*{\file}[1]{\textnormal{\texttt{.#1}}}
%  \newcommand*{\env}[1]{\textnormal{\texttt{#1}}}
%
%
% ^^A -----------------------------
%
%  \title{\unskip
%         The \textsf{multitoc} package^^A
%           \thanks{^^A
%               The version number of this file is \fileversion,
%              revision~\#\gitAbbrevHash, last revised \protect\gitAuthorIsoDate.\protect\newline
%              The name \textsf{multitoc} is a tribute to the $8+3$ 
%              file-naming convention of certain ``operating 
%              systems''; strictly speaking it should be
%              \textsf{MulticolumnToc}.}
%        }
%  \author{Martin Schröder\\
%	\url{https://gitlab.com/TeXhackse/multitoc}\thanks{maintained by Marei Peischl}}
%  \date{\filedate}
%  \maketitle
%
% ^^A -----------------------------
%
% \begingroup\renewcommand*{\abstractname}{Disclaimer/Warning}
% \begin{abstract}
%  \changes{v2.02}{2024/06/13}{Add note, that there will not be any additional features.}
% This package is maintained to keep compatibility but will not receive any further feature extensions as there are a lot of packages providing by far more functionality.
%
% In case you are looking for methods to modify lists have a look at Markus Kohm's tocbasic Package \cite{package:tocbasic} or the corresponding CTAN Topic \url{https://ctan.org/topic/toc-etc}.
% \end{abstract}
% \endgroup
%
%  \begin{abstract}
%     This package allows setting only the table of contents, list of 
%     figures and/or list of tables in two or more columns.
%     The number of columns can be configured via commands; the 
%     multicolumn toc(s) can be selected via package options.
%  \end{abstract}
%
%  \pagestyle{headings}
%
% ^^A -----------------------------
%
%  \tableofcontents
%
% ^^A -----------------------------
%
%  \section{Introduction}
%
%
% ^^A -----------------------------
%
%  \subsection{The problem\label{sec:sec:problem}}
%  ^^A
%  When you want to set only the table of contents of a document in two 
%  (or more columns), there is one known way^^A
%     \footnote{^^A
%        This was first used in the \package{doc} 
%        package\protect\cite{package:doc}.
%     }^^A
%  : Add an
%  \begin{quote}
%  |\addtocontents{toc}{\protect\begin{multicols}{2}}|
%  \end{quote}
%  before the \cs{tableofcontents} and an
%  \begin{quote}
%  |\addtocontents{toc}{\protect\end{multicols}}|
%  \end{quote}
%  at the end of the document.
%  This way your \file{toc} will start with |\begin{multicols}{2}|
%  and end with |\end{multicols}|.
%
%  This can be automised by using \cs{AtBeginDocument} and
%  \cs{AtEndDocument} but this has the drawback that it may
%  collide with other commands inserted using \cs{AtEndDocument} that
%  try to write information to the auxilary file (like the
%  \package{count1to} package\cite{package:count1to}).
%  This is because \TeX{} has two ways to write information to files 
%  \cite[p.\ 226--228]{KnuthTeXa}: immediately (when the \cs{write} is 
%  prefixed by \cs{immediate}) or deferred till the current page is 
%  finished and written out by \TeX{} (at the next \cs{shipout}).
%  \cs{addtocontents} does a deferred \cs{write}, \package{count1to}
%  does an immediate.
%
%  \begin{quote}
%     \small
%     Sidenote: \cs{addtocontents} writes a command to the \file{aux} file
%     to write its information to the \file{toc} file.
%     The \file{aux} is read in and executed by |\end{document}| and at
%     the next run by |\begin{document}|.
%     So the \file{toc} is written by |\end{document}| and at the
%     next run by |\begin{document}|.
%  \end{quote}
%
%  When you use the automised solution with \package{count1to} this may
%  happen:
%  \begin{enumerate}
%     \item The |\addtocontents{toc}{\protect\end{multicols}}| is inserted
%        using \cs{AtEndDocument} somewhere in the preamble.
%     \item \package{count1to} inserts this code
%        \begin{quote}
%        \hfuzz135pt
%        \small
%        |\clearpage|\\
%        |\immediate\write\@auxout{\string\newlabel{TotalPages}{{\the\count1}{\the\count1}}}|
%        \end{quote}
%        via an |\AtBeginDocument{\AtEndDocument{| so it can be sure that
%        this code is the \emph{last} code executed by \cs{AtEndDocument}.
%     \item At the |\end{document}| first the code from step~1 is executed
%        which adds the information to the current page to write the
%        |\@writefile{toc}{\end{multicols}}| to the auxilary file 
%        \emph{when the current page is finished}.
%
%        Then the code from step~2 is executed, which lets \LaTeX{} clear
%        the page (to force all pending writes to be written) and 
%        \emph{immediately} after that write the information for the 
%        |TotalPages| to the auxilary file.
%
%        Now suppose that the |\end{document}| is called \emph{just after}
%        \LaTeX{} has finished the last page.
%        The code from step~1 is never written to the file (since at the
%        \cs{clearpage} \LaTeX{} has no page to finish and so the code 
%        waits for the \cs{shipout} which won't come), but the code from 
%        step~2 is.
%        So you now have an \file{aux} which won't write the
%        |\end{multicols}| to the \file{toc}.
%        This of course produces an error at the next run.
%  \end{enumerate}
%
%
% ^^A -----------------------------
%
%  \subsection{A solution}
%  ^^A
%  \DescribeMacro{\@starttoc}
%  An easy way around this is to simply redefine \cs{@starttoc} from
%  \cite{package:ltsect} which reads in the \file{toc} to wrap a
%  \env{multicol} around it.\footnote{^^A
%     Thanks to Frank Mittelbach for reminding me of this.}^^A
%  $^{,}$^^A
%  \footnote{^^A
%     \raggedright
%     Version~1.00 of this package provided a more elaborate solution
%     to this using \cs{immediateaddtocontents}; this is no longer needed.
%     But the commands from version~1.00 are still provided (see
%     section~\ref{sec:sec:version100}).}
%
%
% ^^A -----------------------------
%
%  \section{Parameters}
%  ^^A
% \DescribeMacro{\multicolumntoc}
% \DescribeMacro{\multicolumnlot}
% \DescribeMacro{\multicolumnlof}
%  The number of columns for the table of contens, list of figures and
%  list of tables can be set by redefining the commands 
%  \cs{multicolumntoc}, \cs{multicolumnlot} and \cs{multicolumnlof}.
%  The default is two columns.
%
%
% ^^A -----------------------------
%
%  \section{Options}
%  ^^A
%  The package has the following options:
%  \nopagebreak
%  \begin{description}
%     \item[\normalfont\option{toc}]
%        The table of contents is set in \cs{multicolumntoc} columns.
%     \item[\normalfont\option{lof}]
%        The list of figures is set in \cs{multicolumnlof} columns.
%     \item[\normalfont\option{lot}]
%        The list of tables is set in \cs{multicolumnlot} columns.
%  \end{description}
%
%
% ^^A -----------------------------
%
%  \section{Required packages}
%  ^^A
%  The package requires the \package{multicol} and the 
%  \package{ifthen} packages.
%
%
% ^^A -----------------------------
%
%  \StopEventually{^^A
%
%
% ^^A -----------------------------
%
%  \begin{thebibliography}{1}
%     \raggedright
%     \bibitem{package:ltfiles}
%        Johannes Braams, David Carlisle, Alan Jeffrey, Leslie Lamport,
%        Frank Mittelbach, Chris Rowley and Rainer Sch\"opf.
%        \newblock \package{ltfiles.dtx}.
%        \newblock \texttt{CTAN: tex-archive/macros/latex/base/ltfiles.dtx}.
%        \newblock \LaTeXe{} kernel.
%     \bibitem{package:ltsect}
%        Johannes Braams, David Carlisle, Alan Jeffrey, Leslie Lamport,
%        Frank Mittelbach, Chris Rowley, Tobias Oetiker and Rainer Sch\"opf.
%        \newblock \package{ltsect.dtx}.
%        \newblock \url{CTAN: tex-archive/macros/latex/base/ltsect.dtx}.
%        \newblock \LaTeXe{} kernel.
%     \bibitem{KnuthTeXa}
%        Donald~E.\ Knuth.
%        \newblock \emph{The {\TeX}Book}, volume~A of \emph{Computers
%           and Typesetting}.
%        \newblock Addison-Wes\-ley, 1986.
%        \newblock Eleventh printing, revised, May 1991.
%     \bibitem{package:doc}
%        Frank Mittelbach.
%        \newblock The \package{doc} and \package{shortvrb} package.
%        \newblock \url{CTAN: tex-archive/macros/latex/base/doc.dtx}.
%        \newblock \LaTeXe{} package.
%     \bibitem{package:count1to}
%        Martin Schr\"oder.
%        \newblock The \package{count1to} package.
%        \newblock \url{CTAN: tex-archive/macros/latex/contrib/supported/ms/count1to.dtx}.
%        \newblock \LaTeXe{} package.
%     \bibitem{package:tocbasic}
%        Markus Kohm
%        \newblock KOMA-Script
%        \newblock \url{CTAN: tex-archive/macros/latex/contrib/koma-script/tocbasic.dtx}.
%        \newblock \LaTeXe{} package.
%  \end{thebibliography}
%
%     }
%
%
% ^^A -----------------------------
%
%  \section{The Implementation}
%  ^^A
%  \setlength{\parindent}{0pt}
%    \begin{macrocode}
%<*package>
%    \end{macrocode}
%
%  We need the \package{multicol} and the \package{ifthen} packages.
%    \begin{macrocode}
\RequirePackage{multicol}
\RequirePackage{ifthen}
%    \end{macrocode}
%
%
% ^^A -----------------------------
%
%  \subsection{The parameters}
%  ^^A
%  \begin{macro}{\multicolumntoc}
%  \begin{macro}{\multicolumnlot}
%  \begin{macro}{\multicolumnlof}
%  \mbox{}
%    \begin{macrocode}
\newcommand{\multicolumntoc}{2}
\newcommand{\multicolumnlot}{2}
\newcommand{\multicolumnlof}{2}
%    \end{macrocode}
%  \end{macro}
%  \end{macro}
%  \end{macro}
%
%
% ^^A -----------------------------
%
%  \subsection{Initial Code}
%  ^^A
%  \begin{macro}{@multitoc@toc}
%     \changes{v2.00}{1998/08/23}{new}
%  \cs{if@multitoc@toc} is used to flag the use of the \option{toc}
%  otion.
%    \begin{macrocode}
\newboolean{@multitoc@toc}
%    \end{macrocode}
%  \end{macro}
%
%  \begin{macro}{@multitoc@lot}
%     \changes{v2.00}{1998/08/23}{new}
%  \cs{if@multitoc@lot} is used to flag the use of the \option{lot}
%  otion.
%    \begin{macrocode}
\newboolean{@multitoc@lot}
%    \end{macrocode}
%  \end{macro}
%
%  \begin{macro}{@multitoc@lof}
%     \changes{v2.00}{1998/08/23}{new}
%  \cs{if@multitoc@lof} is used to flag the use of the \option{lof}
%  otion.
%    \begin{macrocode}
\newboolean{@multitoc@lof}
%    \end{macrocode}
%  \end{macro}
%
%
% ^^A -----------------------------
%
%  \subsection{The options}
%  ^^A
%  The options simply set the corresponding flags.
%  \changes{v2.00}{1998/08/23}{Redesign}
%  \changes{v1.00}{1998/08/09}{\cs{clearpage}}
%    \begin{macrocode}
\DeclareOption{toc}{\setboolean{@multitoc@toc}{true}}
\DeclareOption{lot}{\setboolean{@multitoc@lot}{true}}
\DeclareOption{lof}{\setboolean{@multitoc@lof}{true}}
%    \end{macrocode}
%
%
% ^^A -----------------------------
%
%  \subsection{Executing options}
%  ^^A
%  There are no default options.
%    \begin{macrocode}
\ProcessOptions\relax
%    \end{macrocode}
%
%
% ^^A -----------------------------
%
%  \subsection{Redefining \cs{@starttoc}}
%  ^^A
% \begin{macro}{\@starttoc}
%  From \cite{package:ltsect}:
%  \begin{quote}
% The |\@starttoc|\marg{ext} command is used to define the commands:\\
% |\tableofcontents|, |\listoffigures|, etc.
%
% For example:
% |\@starttoc{lof}| is used in |\listoffigures|.  This command
% reads the |.|\meta{ext} file and sets up to write the new
% |.|\meta{ext} file.
%
% \begin{oldcomments}
% \@starttoc{EXT} ==
%   BEGIN
%     \begingroup
%        \makeatletter
%        read file \jobname.EXT
%        IF @filesw = true
%          THEN  open \jobname.EXT as file \tf@EXT
%        FI
%        @nobreak :=G FALSE  %% added 24 May 89
%     \endgroup
%   END
% \end{oldcomments}
%  \end{quote}
%  We store the current definition in \cs{@multitoc@starttoc}
%  and wrap it in a \env{multicol} environment if the right 
%  option has been selected.
%  We also have to determine the number of columns to use.\footnote{^^A
%     This could probably be coded more effeciently, but this code
%     works\ldots}
%  \changes{v2.02}{2024/06/13}{Fix bug breaking other lists but toc/lof/lot. Thanks to Markus Kohm for reporting.}
%    \begin{macrocode}
\let\@multitoc@starttoc\@starttoc
\ExplSyntaxOn
\renewcommand*{\@starttoc}[1]{%
	\clist_if_in:nnTF {toc,lof,lot} {#1} {
		\ifthenelse{\boolean{@multitoc@#1}}{
			\begin{multicols}{\use:c{multicolumn#1}}%
				\@multitoc@starttoc{#1}%
			\end{multicols}
		}{
		\@multitoc@starttoc{#1}
		}
	}{
	\@multitoc@starttoc{#1}
	}
}
\ExplSyntaxOff
%    \end{macrocode}
% \end{macro}
%
%
% ^^A -----------------------------
%
%  \subsection{Code from Version~1.00\label{sec:sec:version100}}
%  ^^A
%  The first version of this package took a different approach to
%  solving the problem mentioned in section~\ref{sec:sec:problem} and
%  provided the commands \cs{protected@write@immediate} and 
%  \cs{immediateaddtocontents}.
%  Their code has been retained here for compatibilty reasons.
%
%  \DescribeMacro{\immediateaddtocontents}
%  \cs{immediateaddtocontents} is a version of \cs{addtocontents} which 
%  does just what the name implies: Write the information \cs{immediate}.
%
%  \begin{macro}{\protected@write@immediate}
%  This is a changed version of \cs{protected@write} 
%  from~\cite{package:ltfiles}.
%  It takes three arguments: an output stream, some initialization
%  code, and some text to write.
%  It then writes this \emph{immediately}, with
%  appropriate handling of |\protect| and |\thepage|.
%  \changes{v2.00}{1998/09/06}{retained}
%    \begin{macrocode}
\newcommand*{\protected@write@immediate}[3]{%
      \begingroup
       \let\thepage\relax
       #2%
       \let\protect\string
       \edef\reserved@a{\immediate\write#1{#3}}%
       \reserved@a
      \endgroup
      \if@nobreak\ifvmode\nobreak\fi\fi
   }
%    \end{macrocode}
% \end{macro}
%
% \begin{macro}{\immediateaddtocontents}
%  This is a changed version of \cs{addtocontents} 
%  from~\cite{package:ltsect}.
%  The |\immediateaddtocontents{|\meta{table}|}{|\meta{text}|}| command
%  \emph{immediately} adds \meta{text} to the |.|\meta{table} file, 
%  with no page number.
%  \changes{v2.00}{1998/09/06}{retained}
%    \begin{macrocode}
\newcommand*{\immediateaddtocontents}[2]{%
  \protected@write@immediate\@auxout
      {\let\label\@gobble \let\index\@gobble \let\glossary\@gobble}%
      {\string\@writefile{#1}{#2}}}
%    \end{macrocode}
% \end{macro}
%
%    \begin{macrocode}
%</package>
%    \end{macrocode}
%
% ^^A -----------------------------
%
%  \Finale
%
%   \PrintIndex\PrintChanges
%   ^^A Make sure that the index is not printed twice
%   ^^A (ltxdoc.cfg might have a second \PrintIndex command)
%   \let\PrintChanges\relax
%   \let\PrintIndex\relax
