# The `multitoc` package

Copyright (C) Martin Schröder, 1998–1999, Marei Peischl (peiTeX)  <marei@peitex.de>, 2024

 multitoc 2024/06/13 v2.02

***************************************************************************

 This material is subject to the LaTeX Project Public License version 1.3c
 or later. See http://www.latex-project.org/lppl.txt for details.

***************************************************************************

This work has the LPPL maintenance status `maintained'.
This package is maintained but will not receive any additional features anymore.

The Current Maintainer of this work is
   Marei Peischl <marei@peitex.de>.

[Link to the GitLab Repository](https://gitlab.com/TeXhackse/ragged2e)

## Abstract
This package only exists and is maintained for compatibility reasons!

The package automatically sets the table of contents, list of figures and list of tables in two or more columns (the number of columns may be configured).

The package uses the multicol package.

## Provided files

* README.md
* multitoc.dtx
* multitoc.ins

The file
* multitoc.sty

## Issue Tracker
If you think you may have found a bug in this package, please visit
  https://gitlab.com/TeXhackse/multitoc/issues

## Version History
   * 2.02 Bugfix to no longer break other lists but toc/lot and lof
t.